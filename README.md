# Spike Report

## SPIKE 6

### Introduction

This spike involves packaging the past 2 spikes into a single game.

### Goals

- Package your project into an Executable.
- Document what options you selected, if any, and what effects they had on either the Packaging process or the Execution of the game (i.e. frame-rate differences, load-time, or packaging-time).


### Personnel

* Primary - Mitchell
* Secondary - N/A

### Technologies, Tools, and Resources used

- [Packaging Projects in Unreal](https://docs.unrealengine.com/latest/INT/Engine/Basics/Projects/Packaging/)

### Tasks Undertaken

1. Followed Packaging Projects in Unreal.
    - Navigate to package settings
        - File -> Package Project -> Packaging Settings...
    - Use Pak file, Include Prerequisites.
    - File -> Package Project -> Windows -> Windows (32-bit)/Windows (64-bit)
    - Save packaged file in appropriate location.

### What we found out

- Pak files are used to compress large files into a single folder. This makes it quicker to package the project as opposed to the uncompressed files.